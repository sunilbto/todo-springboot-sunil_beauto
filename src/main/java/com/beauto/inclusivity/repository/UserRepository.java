package com.beauto.inclusivity.repository;

import org.springframework.data.repository.CrudRepository;

import com.beauto.inclusivity.model.User;

public interface UserRepository extends CrudRepository<User, Long>{

}
